# LEAP MOTION CONTROLLER CON PYTHON 2.7 EN WINDOWS

Este repositorio contiene un script básico de lectura de datos RAW para el sensor LEAP MOTION CONTROLLER. Se corre por ahora bajo el sistema operativo Windows 10 por el .dll proporcionado para dicha conexión.

Para correr de manera correcta este script es necesario realizar la instalación previa de los drivers y el SDK del LeapMotionController en Windows. Descargar del siguiente link: 

[Leap Motion SDK V2](https://developer.leapmotion.com/setup/desktop) 

Si se tienen problemas con el driver, ejecutar los drivers contenidos en la carpeta "Driver" en la raiz de instalación del SDK.


## Prerrequisitos

* Python 2.7 

Para ejecutar este script se debe conectar el LeapMotionController al PC a través del cable USB y en una consola de windows ejecutar el siguiente comando

```/directorio_proyecto```

+ Comprobar que está ejecutando python 2.7

        python LeapMotionRAW.py


## Resultados

Este Script tiene como salida lo siguiente : 

* Lectura de manos, cantidad de dedos y gestos: 
  
  ![Lectura de manos, cantidad de dedos y gestos](./Imagenes/manos_dedos_gestos.PNG)

  Estas líneas de código permiten obtener la información por cada frame de cuantas manos y cuantos dedos se están detectando, así mismo el número de herramientas y gestos que se realizan.

* Lectura de posición de la palma:  
  ![Lectura de manos, cantidad de dedos y gestos](./Imagenes/2_posicionpalma.PNG)

  Esta parte del script permite obtener un ID independiente por cada una de las manos que son detectadas en el sensor además de tener la posición en XYZ de la palma de la mano.

* Lectura de inclinación de la palma respecto a la muñeca:  
  ![Lectura de manos, cantidad de dedos y gestos](./Imagenes/3_pitch_roll_yaw.PNG)

  Se obtiene también la inclinación de la mano respecto a la muñeca en pitch, roll y yaw.

* Lectura de posición de muñeca, codo y dirección de brazo
  ![Lectura de manos, cantidad de dedos y gestos](./Imagenes/4_muneca_codo_dirbrazo.PNG)

  También este scritp consigue la posición en el plano XYZ de la muñeca, codo y la dirección del vector del antebrazo.

*  Lectura de dedos y tamaños
  
  ![Lectura de manos, cantidad de dedos y gestos](./Imagenes/5_dedos_nombres_tamaños.PNG)

   Se pueden obtener también todos los tamaños de los dedos de todas las manos detectadas en el sensor.

* Inicio y fin de cada hueso
  ![Lectura de manos, cantidad de dedos y gestos](./Imagenes/6-huesos.PNG)

  También el script es capaz de reconocer todos los huesos que componen cada dedo de la mano y marca su coordenada de inicio y su coordenada final en el plano XYZ.